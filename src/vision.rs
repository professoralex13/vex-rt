//! # Vision Sensor API

use alloc::{format, vec::Vec};

use crate::{
    bindings,
    error::{get_errno, Error},
};

/// A struct representing a signature which can be added to a
/// [`VisionSensor`](crate::vision::VisionSensor)
pub struct VisionSignature {
    /// The id of the signature, 1-8 inclusive
    pub id: u8,
    /// The minimum u of the signature
    pub u_min: i32,
    /// The maximum u of the signature
    pub u_max: i32,
    /// The mean u of the signature
    pub u_mean: i32,
    /// The minimum v of the signature
    pub v_min: i32,
    /// The maximum v of the signature
    pub v_max: i32,
    /// The mean v of the signature
    pub v_mean: i32,
    /// The range of the signature
    pub range: f32,
    /// The type of the signature
    pub signature_type: i32,
}

/// The type of a returned VisionObject
#[derive(Copy, Clone, Debug)]
pub enum VisionObjectType {
    /// A normal object based on an added signature
    Normal,
    /// A Color code object type
    ColorCode,
    /// A line object type
    Line,
}

// Im sorry these docs can only be as good at the pros docs

impl TryFrom<u32> for VisionObjectType {
    type Error = VisionSensorError;
    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Normal),
            1 => Ok(Self::ColorCode),
            2 => Ok(Self::Line),
            _ => Err(VisionSensorError::UnknownObjectType(value)),
        }
    }
}

/// An Object detected by the [`VisionSensor`](crate::vision::VisionSensor)
/// based upon added signatures
#[derive(Copy, Clone, Debug)]
pub struct VisionObject {
    /// The id of the signature this object was detected with
    pub signature_id: u16,
    /// The type of object
    pub object_type: VisionObjectType,

    /// The x pixel coordinate of the center of the object
    pub center_x_coordinate: i16,
    /// They y pixel coordinate of the center of the object
    pub center_y_coordinate: i16,
    /// The pixel width of the object
    pub width: i16,
    /// The pixel height of the object
    pub height: i16,

    /// The angle of a color code object in 0.1 degree units (e.g. 10 -> 1
    /// degree, 155 -> 15.5 degrees)
    pub angle: u16,
}

impl TryFrom<bindings::vision_object> for VisionObject {
    type Error = VisionSensorError;

    fn try_from(value: bindings::vision_object) -> Result<Self, Self::Error> {
        Ok(Self {
            signature_id: value.signature,
            object_type: value.type_.try_into()?,
            center_x_coordinate: value.x_middle_coord,
            center_y_coordinate: value.y_middle_coord,
            width: value.width,
            height: value.height,
            angle: value.angle,
        })
    }
}

/// A struct representing a connected VisionSensor which can be used for
/// detecting visible objects based on added signatures
pub struct VisionSensor {
    port: u8,
    signatures: [Option<bindings::vision_signature>; 8],
}

impl VisionSensor {
    /// Constructs a new vision sensor.
    ///
    /// # Safety
    ///
    /// This function is unsafe because it allows the user to create multiple
    /// mutable references to the same vision sensor. You likely want to
    /// implement [`Robot::new()`](crate::robot::Robot::new()) instead.
    pub unsafe fn new(port: u8) -> Self {
        Self {
            port,
            signatures: Default::default(),
        }
    }

    /// Gets the number of objects currently detected by the
    /// [`VisionSensor`](crate::vision::VisionSensor)
    pub fn get_object_count(&self) -> Result<u32, VisionSensorError> {
        unsafe {
            match bindings::vision_get_object_count(self.port) {
                bindings::PROS_ERR_ => Err(VisionSensorError::from_errno()),
                x => Ok(x as u32),
            }
        }
    }

    /// Gets all objects currently detected by the
    /// [`VisionSensor`](crate::vision::VisionSensor)
    pub fn get_objects(&self) -> Result<Vec<VisionObject>, VisionSensorError> {
        unsafe {
            let mut object_vec: Vec<bindings::vision_object> = Vec::new();

            let object_count = self.get_object_count()?;

            for i in 0..object_count {
                let object = bindings::vision_get_by_size(self.port, i);

                object_vec.push(object);
            }

            object_vec.into_iter().map(TryInto::try_into).collect()
        }
    }

    /// Stores a signature to the [`VisionSensor`](crate::vision::VisionSensor)
    /// to allow detection of objects matching the given signature
    pub fn add_signature(&mut self, signature: VisionSignature) -> Result<(), VisionSensorError> {
        unsafe {
            let array_index = signature.id as usize;

            if self.signatures[array_index].is_some() {
                return Err(VisionSensorError::SignatureIdAlreadAssigned);
            }

            let VisionSignature {
                id,
                u_min,
                u_max,
                u_mean,
                v_min,
                v_max,
                v_mean,
                range,
                signature_type,
            } = signature;

            let converted_signature = bindings::vision_signature_from_utility(
                id as i32,
                u_min,
                u_max,
                u_mean,
                v_min,
                v_max,
                v_mean,
                range,
                signature_type,
            );

            self.signatures[array_index] = Some(converted_signature);

            let signature_ptr = &mut self.signatures[array_index].unwrap();

            match bindings::vision_set_signature(self.port, signature.id, &mut *signature_ptr) {
                bindings::PROS_ERR_ => Err(VisionSensorError::from_errno()),
                _ => Ok(()),
            }
        }
    }
}

/// Represents the possible errors that can occur during
/// [`VisionSensor`](crate::vision::VisionSensor) operation
#[derive(Debug)]
pub enum VisionSensorError {
    /// Port is out of range (1-21).
    PortOutOfRange,
    /// Port cannot be configured as a
    /// [`VisionSensor`](crate::vision::VisionSensor).
    PortNotVisionSensor,
    /// The provided signature id is out of range (1-8)
    SignatureIdOutOfRange,
    /// The provided signature id is already used by another added signature
    SignatureIdAlreadAssigned,
    /// The object type of the returned object was not an expected value
    UnknownObjectType(u32),
    /// Unknown error
    Unknown(i32),
}

impl VisionSensorError {
    fn from_errno() -> Self {
        match get_errno() {
            libc::ENXIO => Self::PortOutOfRange,
            libc::ENODEV => Self::PortNotVisionSensor,
            libc::EINVAL => Self::SignatureIdOutOfRange,
            x => Self::Unknown(x),
        }
    }
}

impl From<VisionSensorError> for Error {
    fn from(err: VisionSensorError) -> Self {
        match err {
            VisionSensorError::PortOutOfRange => Error::Custom("port out of range".into()),
            VisionSensorError::PortNotVisionSensor => {
                Error::Custom("port not a vision sensor".into())
            }
            VisionSensorError::SignatureIdOutOfRange => {
                Error::Custom("signature id is not in range 1-7".into())
            }
            VisionSensorError::SignatureIdAlreadAssigned => {
                Error::Custom("Signature id already assigned".into())
            }
            VisionSensorError::UnknownObjectType(x) => {
                Error::Custom(format!("Unknown Object Type: {x}"))
            }
            VisionSensorError::Unknown(n) => Error::System(n),
        }
    }
}
